package pa.soft.model;


import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ConstructionTypeTest
{
	@Test
	void isBuildableWith_givenOwnBaustoffe_returnsTrue()
	{
		for(ConstructionType constructionType : ConstructionType.values())
		{
			final Map<BaustoffType, Integer> containedBaustoffe = constructionType.getBaustoffMap();

			assertTrue(constructionType.isBuildableWith(containedBaustoffe));
		}
	}

	@Test
	void isBuildableWith_givenSixBaustoffeContainingCorrect_returnsTrue()
	{
		for(ConstructionType constructionType : ConstructionType.values())
		{
			final Map<BaustoffType, Integer> containedBaustoffe = giveSixRandomRight(constructionType.getBaustoffe());

			assertTrue(constructionType.isBuildableWith(containedBaustoffe));
		}
	}

	@Test
	void siedlungIsBuildableWith_givenRandomBaustoffe_findBug()
	{
		for (int i = 0; i < 1_000; i++)
		{
			Map<BaustoffType, Integer> randomBaustoffe = giveSixRandom();
			if(ConstructionType.STADT.isBuildableWith(randomBaustoffe))
			{
				System.out.println("true");
				System.out.println(randomBaustoffe);
			}
		}
	}

	private Map<BaustoffType, Integer> giveSixRandomRight(List<BaustoffType> containedBaustoffe)
	{
		List<BaustoffType> sixRightBaustoffe = new ArrayList<>(containedBaustoffe);
		for (int i = containedBaustoffe.size(); i < 6; i++)
		{
			sixRightBaustoffe.add(BaustoffType.pickRandom());
		}

		return buildBaustoffeMap(sixRightBaustoffe);
	}

	private Map<BaustoffType, Integer> giveSixRandom()
	{
		List<BaustoffType> sixRandomBaustoffe = new ArrayList<>();
		for (int i = 0; i < 6; i++)
		{
			sixRandomBaustoffe.add(BaustoffType.pickRandom());
		}

		return buildBaustoffeMap(sixRandomBaustoffe);
	}

	private Map<BaustoffType, Integer> buildBaustoffeMap(final List<BaustoffType> sixRightBaustoffe)
	{
		Map<BaustoffType, Integer> baustoffMap = new HashMap<>();
		for(BaustoffType baustoffType : sixRightBaustoffe)
		{
			int amount = baustoffMap.getOrDefault(baustoffType, 0);
			amount++;
			baustoffMap.put(baustoffType, amount);
		}

		return baustoffMap;
	}
}
