package pa.soft.model;

import java.util.Random;

public enum BaustoffType
{
	GETREIDE("Getreide"),
	GOLD("Gold"),
	HOLZ("Holz"),
	LEHM("Lehm"),
	SCHAF("Schaf"),
	STEIN("Stein");

	private static final Random random = new Random();

	private final String name;

	BaustoffType(String name)
	{
		this.name = name;
	}

	public String getIconFileName()
	{
		return name.toLowerCase() + ".png";
	}

	public static BaustoffType pickRandom()
	{
		return values()[random.nextInt(values().length)];
	}
}
