package pa.soft.model;

import javafx.scene.image.Image;

public class ResourceImage
{
	private final BaustoffType baustoffType;
	private final Image image;

	public ResourceImage(final BaustoffType baustoffType, final Image image)
	{
		this.baustoffType = baustoffType;
		this.image = image;
	}

	public BaustoffType getBaustoffType()
	{
		return baustoffType;
	}

	public Image getImage()
	{
		return image;
	}
}
