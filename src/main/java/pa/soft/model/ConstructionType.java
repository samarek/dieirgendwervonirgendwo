package pa.soft.model;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public enum ConstructionType
{
	STRASSE("Strasse", initStrasseResourceMap()),
	RITTER("Ritter", initRitterResourceMap()),
	SIEDLUNG("Siedlung", initSiedlungResourceMap()),
	STADT("Stadt", initStadtResourceMap());

	private final String name;
	private final Map<BaustoffType, Integer> baustoffe;

	ConstructionType(String name, Map<BaustoffType, Integer> baustoffe)
	{
		this.name = name;
		this.baustoffe = baustoffe;
	}

	public String getName()
	{
		return name;
	}

	public List<BaustoffType> getBaustoffe()
	{
		List<BaustoffType> resourceList = new ArrayList<>();

		for(Map.Entry<BaustoffType, Integer> entry : baustoffe.entrySet())
		{
			for (int i = 0; i < entry.getValue(); i++)
			{
				resourceList.add(entry.getKey());
			}
		}

		return resourceList;
	}

	public Map<BaustoffType, Integer> getBaustoffMap()
	{
		return baustoffe;
	}

	public String getIconFileName()
	{
		String tempString = "";
		if(this.equals(RITTER))
		{
			tempString = "_TEMP";
		}
		return name.toLowerCase() + tempString + ".png";
	}

	private static Map<BaustoffType, Integer> initStrasseResourceMap()
	{
		Map<BaustoffType, Integer> resources = new EnumMap<>(BaustoffType.class);
		resources.put(BaustoffType.LEHM, 1);
		resources.put(BaustoffType.HOLZ, 1);

		return resources;
	}

	private static Map<BaustoffType, Integer> initRitterResourceMap()
	{
		Map<BaustoffType, Integer> resources = new EnumMap<>(BaustoffType.class);
		resources.put(BaustoffType.STEIN, 1);
		resources.put(BaustoffType.SCHAF, 1);
		resources.put(BaustoffType.GETREIDE, 1);

		return resources;
	}

	private static Map<BaustoffType, Integer> initSiedlungResourceMap()
	{
		Map<BaustoffType, Integer> resources = new EnumMap<>(BaustoffType.class);
		resources.put(BaustoffType.LEHM, 1);
		resources.put(BaustoffType.HOLZ, 1);
		resources.put(BaustoffType.SCHAF, 1);
		resources.put(BaustoffType.GETREIDE, 1);

		return resources;
	}

	private static Map<BaustoffType, Integer> initStadtResourceMap()
	{
		Map<BaustoffType, Integer> resources = new EnumMap<>(BaustoffType.class);
		resources.put(BaustoffType.STEIN, 3);
		resources.put(BaustoffType.GETREIDE, 2);

		return resources;
	}

	public boolean isBuildableWith(Map<BaustoffType, Integer> providedResources)
	{
		Map<BaustoffType, Integer> stillNeededResources = subtractfromNeededResources(providedResources);
		int goldAmount = providedResources.getOrDefault(BaustoffType.GOLD, 0);
		boolean goldIsSufficient = goldAmount > 0 && goldIsSufficient(stillNeededResources, goldAmount);

		return stillNeededResources.isEmpty() || goldIsSufficient;
	}

	private Map<BaustoffType, Integer> subtractfromNeededResources(final Map<BaustoffType, Integer> providedResources)
	{
		Map<BaustoffType, Integer> stillNeededRessources = new EnumMap<>(BaustoffType.class);

		for(Map.Entry<BaustoffType, Integer> entry : baustoffe.entrySet())
		{
			BaustoffType currentKey = entry.getKey();
			Integer currentValue = entry.getValue();

			currentValue -= providedResources.getOrDefault(currentKey, 0);

			if(currentValue > 0)
			{
				stillNeededRessources.put(currentKey, currentValue);
			}
		}

		return stillNeededRessources;
	}

	private boolean goldIsSufficient(final Map<BaustoffType, Integer> neededResources, final int goldAmount)
	{
		int totalNeeded = neededResources.values().stream().mapToInt(Integer::intValue).sum() * 2;

		return goldAmount >= totalNeeded;
	}

	@Override
	public String toString()
	{
		return "ConstructionType{" + "name='" + name + '\'' + ", baustoffe=" + baustoffe + "}";
	}
}
