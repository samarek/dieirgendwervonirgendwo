package pa.soft.util;

import javafx.scene.image.Image;
import pa.soft.model.BaustoffType;
import pa.soft.model.ResourceImage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ResourceProvider
{
	public Image provideNoResourceIcon()
	{
		return provideImage("/icons/question_mark.png");
	}

	public ResourceImage provideResourceIcon(final BaustoffType baustoffType)
	{
		return provideResourceIcons(baustoffType).get(0);
	}

	public List<ResourceImage> provideResourceIcons()
	{
		return provideResourceIcons(BaustoffType.values());
	}

	private List<ResourceImage> provideResourceIcons(BaustoffType... baustoffTypes)
	{
		final List<ResourceImage> iconImages = new ArrayList<>();

		for(BaustoffType baustoffType : baustoffTypes)
		{
			iconImages.add(new ResourceImage(baustoffType, provideImage("/icons/" + baustoffType.getIconFileName())));
		}

		return iconImages;
	}

	public Image provideStrassenImage(int strassenIndex)
	{
		return provideImage("/icons/strassen/strasse_" + strassenIndex + ".png");
	}

	public Image provideConstructionImage(String imageName)
	{
		return provideImage("/icons/constructions/" + imageName);
	}

	public Image provideImage(String imagePath)
	{
		String iconsResource = getClass().getResource(imagePath).toExternalForm();

		return new Image(iconsResource);
	}

	public Image provideLockIcon(boolean closed)
	{
		if(closed)
		{
			return provideImage("/icons/lock/lock.png");
		}
		else
		{
			return provideImage("/icons/lock/unlock.png");
		}
	}

	public File provideFile(String filePath)
	{
		return new File(getClass().getResource(filePath).getFile());
	}
}
