package pa.soft.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Set;

public class Logger
{
	private final DateTimeFormatter formatterIso8601 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	private final String logFilePath;

	public Logger(final String logFilePath)
	{
		this.logFilePath = logFilePath;
	}

	public void logException(Exception e)
	{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String sStackTrace = sw.toString();

		log("### Exception ###" + System.lineSeparator() + sStackTrace);
	}

	public void logMap(Map<String, String> map)
	{
		Set<Map.Entry<String, String>> entries = map.entrySet();

		StringBuilder mapString = new StringBuilder("### Map ###" + System.lineSeparator());
		for(Map.Entry<String, String> entry : entries)
		{
			mapString.append(entry.getKey()).append(";").append(entry.getValue()).append(System.lineSeparator());
		}
		log(mapString.toString());
	}

	private void log(String message)
	{
		String logEntry = makeTimestamp() + " -> " + message;
		try(FileWriter fw = new FileWriter(new File(logFilePath), true))
		{
			fw.write(logEntry + System.lineSeparator());
			fw.flush();
		}
		catch(IOException e)
		{
			logException(e);
		}
	}

	private String makeTimestamp()
	{
		return LocalDateTime.now().format(formatterIso8601);
	}
}
