package pa.soft.view;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import pa.soft.control.ConstructionControl;
import pa.soft.control.actions.DiceAction;
import pa.soft.model.BaustoffType;
import pa.soft.view.diceview.BaukostenView;
import pa.soft.view.diceview.BaustoffDice;

import java.util.List;

public class DiceView
{
	private Label rollsRemainingLabel;
	private final IntegerProperty rollsRemainingProperty = new SimpleIntegerProperty(3);

	private final BaukostenView baukostenView;
	private final BaustoffDice baustoffDice;
	private final ConstructionControl constructionControl;
	private final Button wuerfeln;
	private final Button rundeBeenden;

	private final BorderPane viewNode = new BorderPane();

	public DiceView(DiceAction rollDice, DiceAction endRound, ConstructionControl constructionControl)
	{
		super();
		wuerfeln = new Button("Würfeln");
		wuerfeln.disableProperty().bind(rollsRemainingProperty.greaterThan(0).not());
		rundeBeenden = new Button("Runde beenden");

		this.baustoffDice = new BaustoffDice();
		this.constructionControl = constructionControl;
		baukostenView = new BaukostenView();

		rollsRemainingProperty.addListener((observable, oldValue, newValue) -> updateRollsRemaining());

		buildLayout(rollDice, endRound);

		updateRollsRemaining();
		resetResources();
	}

	private void buildLayout(final DiceAction rollDice, final DiceAction endRound)
	{
		viewNode.setTop(baukostenView.getViewNode());
		viewNode.setCenter(generateCenter());
		placeRundeBeendenButton(endRound);
		viewNode.setStyle("-fx-padding: 15px; -fx-border-color: lightgray;");
		wuerfeln.setOnAction(event -> rollDice.execute());
	}

	private void placeRundeBeendenButton(final DiceAction endRound)
	{
		rundeBeenden.setOnAction(event -> endRound.execute());
		rundeBeenden.setMaxWidth(Double.MAX_VALUE);
		viewNode.setBottom(rundeBeenden);
		BorderPane.setAlignment(rundeBeenden, Pos.BOTTOM_CENTER);
	}

	private VBox generateCenter()
	{
		VBox vBox = new VBox();

		vBox.getChildren().add(baustoffDice.getViewNode());
		HBox roundRollPanel = generateRoundRollPanel();
		vBox.getChildren().add(roundRollPanel);
		vBox.getChildren().add(constructionControl.getView());

		return vBox;
	}

	private HBox generateRoundRollPanel()
	{
		HBox roundRollPanel = new HBox();
		roundRollPanel.setAlignment(Pos.CENTER);
		roundRollPanel.setStyle("-fx-padding: 10px 0 10px 0;");

		roundRollPanel.getChildren().add(wuerfeln);
		rollsRemainingLabel = new Label();
		rollsRemainingLabel.setStyle("-fx-padding: 5px;");
		roundRollPanel.getChildren().add(rollsRemainingLabel);

		return roundRollPanel;
	}

	private void updateRollsRemaining()
	{
		rollsRemainingLabel.setText("Würfe übrig " + rollsRemainingProperty.getValue());
	}

	public void rollDice()
	{
		baustoffDice.rollDice();
	}

	public void removeBaustoffe(List<BaustoffType> baustoffeToRemove)
	{
		baustoffDice.removeBaustoffe(baustoffeToRemove);
	}

	public IntegerProperty rollsRemainingProperty()
	{
		return rollsRemainingProperty;
	}

	public void resetResources()
	{
		baustoffDice.resetBaustoffe();
	}

	public Node getViewNode()
	{
		return viewNode;
	}

	public List<BaustoffType> getUnusedBaustoffe()
	{
		return baustoffDice.getUnusedBaustoffe();
	}

	public void replaceBaustoff(final BaustoffType baustoffType, final BaustoffType replaceWithBaustoffType)
	{
		baustoffDice.replaceBaustoff(baustoffType, replaceWithBaustoffType);
	}
}
