package pa.soft.view;

import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import pa.soft.control.DiceControl;
import pa.soft.control.PlayerControl;

public class MainView
{
	private final DiceControl diceControl;
	private final PlayerControl playerControl;

	private final BorderPane viewNode = new BorderPane();

	public MainView(final DiceControl diceControl, final PlayerControl playerControl)
	{
		super();

		this.diceControl = diceControl;
		this.playerControl = playerControl;

		viewNode.setLeft(this.diceControl.getView());
		viewNode.setCenter(this.playerControl.getView());
	}

	public Node getViewNode()
	{
		return viewNode;
	}
}
