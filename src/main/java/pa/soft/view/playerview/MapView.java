package pa.soft.view.playerview;

import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import pa.soft.util.ResourceProvider;

public class MapView
{
	private final ResourceProvider resourceProvider = new ResourceProvider();

	private final StackPane viewNode = new StackPane();

	public MapView(final Pane strassenLayer, final Pane ritterLayer, final Pane siedlungLayer, final Pane stadtLayer, final Pane resourceLayer)
	{
		super();

		final ImageView map = new ImageView(resourceProvider.provideImage("/Karte.png"));
		viewNode.setMaxHeight(map.getFitHeight());
		viewNode.setMaxWidth(map.getFitWidth());
		viewNode.getChildren().add(map);

		viewNode.getChildren().addAll(strassenLayer, ritterLayer, siedlungLayer, stadtLayer, resourceLayer);
	}

	public StackPane getViewNode()
	{
		return viewNode;
	}
}
