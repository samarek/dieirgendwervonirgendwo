package pa.soft.view.playerview.map;

import javafx.geometry.Point2D;
import javafx.scene.layout.StackPane;

public abstract class MapObject
{
	protected final StackPane viewNode = new StackPane();

	public void setPosition(Point2D position)
	{
		viewNode.setLayoutX(position.getX());
		viewNode.setLayoutY(position.getY());
	}

	public StackPane getViewNode()
	{
		return viewNode;
	}
}
