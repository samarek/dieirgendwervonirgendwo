package pa.soft.view.playerview.map;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import pa.soft.control.actions.RitterBaustoffAction;
import pa.soft.model.ResourceImage;
import pa.soft.model.BaustoffType;
import pa.soft.util.ResourceProvider;

public class RitterBaustoff extends MapObject
{
	private static final double DEFAULT_RADIUS = 25;
	private final ResourceProvider resourceProvider = new ResourceProvider();
	private final BooleanProperty isActive = new SimpleBooleanProperty(false);
	private final BooleanProperty wasUsed = new SimpleBooleanProperty(false);
	private final RitterBaustoffAction ritterBaustoffAction;
	private final BaustoffType baustoffType;

	public RitterBaustoff(BaustoffType baustoffType, final RitterBaustoffAction ritterBaustoffAction)
	{
		super();
		this.baustoffType = baustoffType;
		this.ritterBaustoffAction = ritterBaustoffAction;

		final Circle imageFrame = generateImageInFrame();
		final Circle imageActiveOverlay = generateOverlay();
		final ImageView crossoutImage = generateCrossout();

		viewNode.getChildren().addAll(imageFrame, imageActiveOverlay, crossoutImage);
		makeViewNodeClickable();
	}

	private Circle generateImageInFrame()
	{
		final Circle imageFrame = new Circle(DEFAULT_RADIUS);
		final ResourceImage resourceImage = resourceProvider.provideResourceIcon(baustoffType);
		imageFrame.setFill(new ImagePattern(resourceImage.getImage()));
		imageFrame.setStroke(Color.BLACK);
		imageFrame.setStrokeWidth(1.5);
		return imageFrame;
	}

	private Circle generateOverlay()
	{
		final Circle imageActiveOverlay = new Circle(DEFAULT_RADIUS);
		imageActiveOverlay.setFill(Color.WHITE);
		imageActiveOverlay.visibleProperty().bind(isActive.not());
		return imageActiveOverlay;
	}

	private ImageView generateCrossout()
	{
		final ImageView crossoutImage = new ImageView(resourceProvider.provideImage("/cross-out.png"));
		crossoutImage.visibleProperty().bind(wasUsed);
		return crossoutImage;
	}

	private void makeViewNodeClickable()
	{
		viewNode.setOnMouseClicked(event -> {
			if(isActive.getValue() && ritterBaustoffAction.useRitterBaustoff(baustoffType))
			{
				wasUsed.setValue(true);
			}
		});
	}

	public BooleanProperty isActiveProperty()
	{
		return isActive;
	}

	public BooleanProperty wasUsedProperty()
	{
		return wasUsed;
	}

	public BaustoffType getBaustoffType()
	{
		return baustoffType;
	}
}
