package pa.soft.view.playerview.map;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.Label;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;
import pa.soft.model.ConstructionType;
import pa.soft.util.ResourceProvider;

public class Construction extends MapObject
{
	private final ImageView constructionIcon;
	private final ColorAdjust greenout;
	private final ColorAdjust blackout;
	private final int punkte;
	private final ResourceProvider resourceProvider = new ResourceProvider();
	private final BooleanProperty isConstructable = new SimpleBooleanProperty(false);
	private final BooleanProperty isConstructed = new SimpleBooleanProperty(false);

	public Construction(ConstructionType constructionType, int punkte)
	{
		super();

		this.punkte = punkte;
		greenout = new ColorAdjust();
		greenout.setHue(.85);
		greenout.setSaturation(1);
		blackout = new ColorAdjust();
		blackout.setBrightness(-1.0);

		constructionIcon = generateConstructionIcon(constructionType);
		Label punkteLabel = generatePunkteLabel();

		viewNode.getChildren().addAll(constructionIcon, punkteLabel);
	}

	private ImageView generateConstructionIcon(final ConstructionType constructionType)
	{
		ImageView icon = new ImageView();
		icon.setImage(resourceProvider.provideConstructionImage(constructionType.getIconFileName()));
		icon.setFitHeight(50);
		icon.setFitWidth(50);
		icon.setEffect(blackout);
		isConstructed.addListener((observable, oldValue, newValue) -> blackout.setBrightness(0.0));
		isConstructable.addListener((observable, oldValue, newValue) -> applyEffect(newValue));

		return icon;
	}

	private void applyEffect(final Boolean newValue)
	{
		if(newValue)
		{
			constructionIcon.setEffect(greenout);
		}
		else
		{
			constructionIcon.setEffect(blackout);
		}
	}

	private Label generatePunkteLabel()
	{
		Label punkteLabel = new Label(punkte + "");
		punkteLabel.setStyle("-fx-text-fill: #fff;");

		return punkteLabel;
	}

	public int construct()
	{
		isConstructed.setValue(true);
		return punkte;
	}

	public BooleanProperty isConstructed()
	{
		return isConstructed;
	}

	public void toggleIsConstructable()
	{
		isConstructable.setValue(!isConstructable.getValue());
	}

	@Override
	public String toString()
	{
		return "Construction{" + "punkte=" + punkte + ", isConstructed=" + isConstructed + "} " + super.toString();
	}

	public void setRotate(final Double rotate)
	{
		viewNode.setRotate(rotate);
	}
}
