package pa.soft.view.playerview;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import pa.soft.util.ResourceProvider;
import pa.soft.view.instructions.InstructionStage;

public class HeaderPane
{
	private final IntegerProperty punkte = new SimpleIntegerProperty(0);

	private final HBox viewNode = new HBox();

	public HeaderPane()
	{
		viewNode.setStyle("-fx-padding: 20px;");
		viewNode.setFillHeight(true);
		viewNode.setAlignment(Pos.CENTER_RIGHT);
		HBox.setHgrow(viewNode, Priority.ALWAYS);

		generatePointBox();

		generateInstructions();
	}

	private void generatePointBox()
	{
		HBox pointBox = new HBox();
		Label caption = new Label("Gesamtpunkte");
		caption.setStyle("-fx-padding: 0 15px 0 0");
		pointBox.getChildren().add(caption);


		Label punkteLabel = new Label("0");
		punkte.addListener((observable, oldValue, newValue) -> punkteLabel.setText(newValue + ""));
		pointBox.getChildren().add(punkteLabel);
		viewNode.getChildren().add(pointBox);
		pointBox.setAlignment(Pos.CENTER_LEFT);
		HBox.setHgrow(pointBox, Priority.ALWAYS);
	}

	private void generateInstructions()
	{
		final ResourceProvider resourceProvider = new ResourceProvider();
		ImageView instructions = new ImageView(resourceProvider.provideImage("/instructions.png"));
		instructions.setOnMouseClicked(event -> showInstructions());
		viewNode.getChildren().add(instructions);
		HBox.setHgrow(instructions, Priority.ALWAYS);
	}

	private void showInstructions()
	{
		InstructionStage instructionStage = new InstructionStage();
		instructionStage.show();
	}

	public void addPunkte(final int punkte)
	{
		this.punkte.setValue(this.punkte.getValue() + punkte);
	}

	public HBox getViewNode()
	{
		return viewNode;
	}
}
