package pa.soft.view.playerview.picker;

import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.ImageView;
import pa.soft.model.BaustoffType;

import java.util.Map;

public class BaustoffPicker extends AbstractPicker
{
	public BaustoffPicker(final Map<Integer, BaustoffType> options)
	{
		super();
		setTitle("Baustoff wählen");

		makeButtonBar(options);
	}

	private void makeButtonBar(final Map<Integer, BaustoffType> options)
	{
		options.forEach(this::makeButton);
		pickerButtonBar.getChildren().addAll(pickerButtons);
	}

	private void makeButton(final Integer index, final BaustoffType baustoffType)
	{
		Button button = new Button(LEERZEICHEN);
		button.setId(index + "");
		button.setContentDisplay(ContentDisplay.TOP);
		ImageView baustoffImage = new ImageView(resourceProvider.provideResourceIcon(baustoffType).getImage());
		button.setGraphic(baustoffImage);
		button.setOnAction(event -> clickPickerButton(index, button));
		pickerButtons.add(button);
	}
}
