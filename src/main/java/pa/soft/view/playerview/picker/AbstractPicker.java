package pa.soft.view.playerview.picker;

import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.layout.HBox;
import pa.soft.util.ResourceProvider;

import java.util.ArrayList;
import java.util.List;

abstract class AbstractPicker extends Dialog<Integer>
{
	private static final String HAKEN = "\u2714";
	static final String LEERZEICHEN = "\u0020";

	protected final ResourceProvider resourceProvider = new ResourceProvider();
	final List<Button> pickerButtons = new ArrayList<>();
	final HBox pickerButtonBar = new HBox(5);

	private int currentChoice = 0;

	AbstractPicker()
	{
		ButtonBar buttonBar = (ButtonBar) getDialogPane().lookup(".button-bar");
		buttonBar.setButtonOrder(ButtonBar.BUTTON_ORDER_NONE);
		getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);
		getDialogPane().setContent(pickerButtonBar);

		setResultConverter(param -> param.getButtonData().isDefaultButton() ? currentChoice : 0);
	}

	void clickPickerButton(final Integer index, final Button button)
	{
		currentChoice = index;
		pickerButtons.forEach(pickerButton -> pickerButton.setText(LEERZEICHEN));
		button.setText(HAKEN);
	}
}
