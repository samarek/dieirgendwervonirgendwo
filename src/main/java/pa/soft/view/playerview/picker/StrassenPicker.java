package pa.soft.view.playerview.picker;

import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.image.ImageView;
import pa.soft.view.playerview.map.Construction;

import java.util.Map;

public class StrassenPicker extends AbstractPicker
{
	public StrassenPicker(final Map<Integer, Construction> options)
	{
		super();
		setTitle("Strasse wählen");

		makeButtonBar(options);
	}

	private void makeButtonBar(final Map<Integer, Construction> options)
	{
		options.forEach((integer, construction) -> makeButton(integer));
		pickerButtonBar.getChildren().addAll(pickerButtons);
	}

	private void makeButton(final Integer index)
	{
		Button button = new Button(LEERZEICHEN);
		button.setId(index + "");
		button.setContentDisplay(ContentDisplay.TOP);
		ImageView strassenImage = new ImageView(resourceProvider.provideStrassenImage(index));
		button.setGraphic(strassenImage);
		button.setOnAction(event -> clickPickerButton(index, button));
		pickerButtons.add(button);
	}
}
