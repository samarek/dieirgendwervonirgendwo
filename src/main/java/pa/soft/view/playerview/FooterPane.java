package pa.soft.view.playerview;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.util.ArrayList;
import java.util.List;

public class FooterPane
{
	private final List<Label> rounds = new ArrayList<>();

	private final HBox viewNode = new HBox();

	public FooterPane()
	{
		super();
		generateProgress();
		viewNode.setAlignment(Pos.CENTER);
	}

	private void generateProgress()
	{
		final Insets margin = new Insets(15, 5, 30, 5);

		final HBox gameRounds = generateGameRounds();
		HBox.setMargin(gameRounds, margin);

		final Label rundenfortschritt = new Label("Rundenfortschritt");
		HBox.setMargin(rundenfortschritt, margin);
		viewNode.getChildren().addAll(rundenfortschritt, gameRounds);
	}

	private HBox generateGameRounds()
	{
		for (int i = 0; i < 15; i++)
		{
			Label round = generateRound();
			if(i % 5 == 4)
			{
				round.setStyle(round.getStyle() + "-fx-background-color: lightgray;");
			}
			rounds.add(round);
		}
		HBox gameRounds = new HBox();
		gameRounds.getChildren().addAll(rounds);

		return gameRounds;
	}

	private Label generateRound()
	{
		double preferredSize = 35;

		Label round = new Label();
		round.setStyle("-fx-padding: 5px; -fx-border-color: black; -fx-border-width: 2px; -fx-border-insets: -1px;");
		round.setPrefSize(preferredSize, preferredSize);
		round.setAlignment(Pos.CENTER);

		return round;
	}

	public void setPunkteForRound(int punkte, int round)
	{
		rounds.get(round).setText(punkte + "");
	}

	public HBox getViewNode()
	{
		return viewNode;
	}
}
