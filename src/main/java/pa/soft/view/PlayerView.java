package pa.soft.view;

import javafx.scene.Node;
import javafx.scene.layout.BorderPane;
import pa.soft.control.MapControl;
import pa.soft.view.playerview.FooterPane;
import pa.soft.view.playerview.HeaderPane;
import pa.soft.view.playerview.MapView;

public class PlayerView
{
	private final MapControl mapControl;

	private MapView mapView;
	private HeaderPane headerPane;
	private FooterPane footerPane;

	private final BorderPane viewNode = new BorderPane();

	public PlayerView(final MapControl mapControl)
	{
		this.mapControl = mapControl;

		reset();
	}

	private void initializeComponents()
	{
		headerPane = new HeaderPane();
		mapView = this.mapControl.getNewMapView();
		footerPane = new FooterPane();
	}

	private void putViewNodesOnPane()
	{
		viewNode.setTop(headerPane.getViewNode());
		viewNode.setCenter(mapView.getViewNode());
		viewNode.setBottom(footerPane.getViewNode());
	}

	public void setPunkteForRound(int punkte, int round)
	{
		headerPane.addPunkte(punkte);
		footerPane.setPunkteForRound(punkte, round);
	}

	public void reset()
	{
		initializeComponents();
		putViewNodesOnPane();
	}

	public Node getViewNode()
	{
		return viewNode;
	}
}
