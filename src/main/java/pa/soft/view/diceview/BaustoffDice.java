package pa.soft.view.diceview;

import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import pa.soft.model.BaustoffType;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class BaustoffDice
{
	private static final int ROWS = 3;
	private static final int COLUMNS = 2;
	private final List<BaustoffDie> baustoffDiceList = new ArrayList<>();

	private final VBox viewNode = new VBox();

	public BaustoffDice()
	{
		super();

		GridPane content = new GridPane();

		for(int i = 0; i < COLUMNS; i++)
		{
			for(int j = 0; j < ROWS; j++)
			{
				final BaustoffDie baustoffDie = new BaustoffDie();
				baustoffDie.showImage(i + (j * 2));
				baustoffDiceList.add(baustoffDie);
				content.add(baustoffDie.getViewNode(), i, j);
			}
		}
		content.setAlignment(Pos.TOP_CENTER);
		viewNode.getChildren().add(content);
	}

	public void rollDice()
	{
		for(BaustoffDie baustoffDie : baustoffDiceList)
		{
			if(baustoffDie.isRollable())
			{
				baustoffDie.roll();
			}
		}
	}

	public void removeBaustoffe(List<BaustoffType> baustoffeToRemove)
	{
		final Iterator<BaustoffType> iterator = baustoffeToRemove.iterator();
		while(iterator.hasNext())
		{
			BaustoffType baustoffType = iterator.next();

			final List<BaustoffDie> unusedBaustoffDice = baustoffDiceList.stream()
																		 .filter(baustoffDie -> !baustoffDie.isUsed())
																		 .collect(Collectors.toList());
			for(BaustoffDie baustoffDie : unusedBaustoffDice)
			{
				if(baustoffDie.getSelectedBaustoff().equals(baustoffType))
				{
					baustoffDie.use();
					iterator.remove();
					break;
				}
			}
		}

		removeGold(baustoffeToRemove);
	}

	private void removeGold(final List<BaustoffType> baustoffeToRemove)
	{
		int goldAmountToRemove = baustoffeToRemove.size() * 2;
		for(int i = 0; i < goldAmountToRemove; i++)
		{
			baustoffDiceList.stream()
							.filter(baustoffDie -> !baustoffDie.isUsed())
							.filter(baustoffDie -> baustoffDie.getSelectedBaustoff().equals(BaustoffType.GOLD))
							.findFirst()
							.ifPresent(BaustoffDie::use);
		}
	}

	public void resetBaustoffe()
	{
		for(BaustoffDie baustoffDie : baustoffDiceList)
		{
			baustoffDie.reset();
		}
	}

	public VBox getViewNode()
	{
		return viewNode;
	}

	public List<BaustoffType> getUnusedBaustoffe()
	{
		return baustoffDiceList.stream()
							   .filter(baustoffDie -> !baustoffDie.isUsed())
							   .map(BaustoffDie::getSelectedBaustoff)
							   .collect(Collectors.toList());
	}

	public void replaceBaustoff(final BaustoffType baustoffType, final BaustoffType replaceWithBaustoffType)
	{
		baustoffDiceList.stream()
						.filter(baustoffDie -> !baustoffDie.isUsed())
						.filter(baustoffDie -> baustoffDie.getSelectedBaustoff().equals(baustoffType))
						.findFirst()
						.ifPresent(baustoffDie -> baustoffDie.setSelectedBaustoff(replaceWithBaustoffType));
	}
}
