package pa.soft.view.diceview;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import pa.soft.model.ResourceImage;
import pa.soft.model.BaustoffType;
import pa.soft.util.ResourceProvider;

import java.util.List;
import java.util.Optional;

class BaustoffDie
{
	private final ResourceProvider resourceProvider;
	private final ImageView lock = new ImageView();
	private final ImageView noBaustoff;
	private final ImageView xOut;
	private final BooleanProperty locked = new SimpleBooleanProperty(false);
	private final BooleanProperty used = new SimpleBooleanProperty(false);
	private BaustoffType selectedBaustoff;

	private final StackPane viewNode = new StackPane();

	BaustoffDie()
	{
		super();

		viewNode.setStyle("-fx-padding: 1px; -fx-border-color: black; -fx-border-insets: 1px;");
		resourceProvider = new ResourceProvider();

		addIconImages();
		addLockImage();
		noBaustoff = new ImageView(resourceProvider.provideNoResourceIcon());
		xOut = new ImageView(resourceProvider.provideImage("/cross-out.png"));
		locked.addListener((observable, oldValue, newValue) -> lock.setImage(resourceProvider.provideLockIcon(newValue)));
		used.addListener((observable, oldValue, newValue) -> strikeOut(newValue));
	}

	private void strikeOut(final Boolean newValue)
	{
		if(newValue)
		{
			viewNode.getChildren().add(xOut);
		}
	}

	private void addIconImages()
	{
		final List<ResourceImage> iconImages = resourceProvider.provideResourceIcons();
		for(final ResourceImage iconImage : iconImages)
		{
			final ImageView imageView = new ImageView(iconImage.getImage());
			imageView.setId(iconImage.getBaustoffType().name());
			viewNode.getChildren().add(imageView);
		}
	}

	private void addLockImage()
	{
		lock.setImage(resourceProvider.provideLockIcon(locked.getValue()));
		lock.setOnMouseClicked(event -> locked.setValue(!locked.getValue()));
		viewNode.getChildren().add(lock);
		StackPane.setAlignment(lock, Pos.TOP_RIGHT);
	}

	void showImage(int index)
	{
		if(!locked.getValue() && !used.getValue())
		{
			viewNode.getChildren().removeAll(lock, noBaustoff, xOut);
			viewNode.getChildren().get(index).toFront();
			viewNode.getChildren().add(lock);
		}
	}

	void roll()
	{
		selectBaustoff(BaustoffType.pickRandom());
	}

	BaustoffType getSelectedBaustoff()
	{
		return selectedBaustoff;
	}

	boolean isRollable()
	{
		return !locked.getValue() && !used.getValue();
	}

	void use()
	{
		used.setValue(true);
	}

	void reset()
	{
		locked.setValue(false);
		used.setValue(false);
		resetImageView();
	}

	private void resetImageView()
	{
		final Optional<Node> noBaustoffOptional = viewNode.getChildren().stream()
														  .filter(node -> node.equals(noBaustoff))
														  .findFirst();
		if(!noBaustoffOptional.isPresent())
		{
			viewNode.getChildren().add(noBaustoff);
		}
	}

	void setSelectedBaustoff(final BaustoffType replaceWithBaustoffType)
	{
		selectBaustoff(replaceWithBaustoffType);
	}

	private void selectBaustoff(final BaustoffType randomBaustoffType)
	{
		final Optional<Node> baustoffImageOptional = viewNode.getChildren().stream()
															 .filter(node -> node.getId() != null)
															 .filter(node -> node.getId().equals(randomBaustoffType.name()))
															 .findAny();

		baustoffImageOptional.ifPresent(node -> {
			int resourceIndex = viewNode.getChildren().indexOf(node);
			showImage(resourceIndex);
			this.selectedBaustoff = randomBaustoffType;
		});
	}

	public StackPane getViewNode()
	{
		return viewNode;
	}

	public boolean isUsed()
	{
		return used.get();
	}
}
