package pa.soft.view.diceview;

import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import pa.soft.model.ConstructionType;
import pa.soft.model.BaustoffType;
import pa.soft.util.ResourceProvider;

import java.util.List;
import java.util.stream.Collectors;

public class BaukostenView
{
	private static final String ICONS_PATH = "/icons/";

	private final ResourceProvider resourceProvider = new ResourceProvider();
	private final GridPane viewNode = new GridPane();

	public BaukostenView()
	{
		super();

		viewNode.setStyle("-fx-padding: 0 0 10px 0;");

		int rowIndex = 0;
		for(ConstructionType constructionType : ConstructionType.values())
		{
			viewNode.addRow(rowIndex++, generateConstructionIcon(constructionType), generateCostsPane(constructionType));
		}
	}

	private ImageView generateConstructionIcon(final ConstructionType constructionType)
	{
		return generateImageView(resourceProvider.provideConstructionImage(constructionType.getIconFileName()));
	}

	private List<ImageView> generateImageViews(final ConstructionType constructionType)
	{
		return constructionType.getBaustoffe().stream()
							   .map(this::generateResourceIcon)
							   .collect(Collectors.toList());
	}

	private Node generateCostsPane(final ConstructionType constructionType)
	{
		List<ImageView> imageViewList = generateImageViews(constructionType);
		final HBox hBox = new HBox();
		hBox.getChildren().addAll(imageViewList);
		hBox.setStyle("-fx-padding: 0 0 0 5px;");

		return hBox;
	}

	private ImageView generateResourceIcon(BaustoffType baustoffType)
	{
		Image baustoffImage = resourceProvider.provideImage(ICONS_PATH + baustoffType.getIconFileName());

		return generateImageView(baustoffImage);
	}

	private ImageView generateImageView(final Image image)
	{
		final ImageView imageView = new ImageView(image);
		imageView.setFitHeight(40);
		imageView.setFitWidth(40);

		return imageView;
	}

	public Node getViewNode()
	{
		return viewNode;
	}
}
