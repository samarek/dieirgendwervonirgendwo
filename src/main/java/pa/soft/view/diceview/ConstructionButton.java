package pa.soft.view.diceview;

import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import pa.soft.control.actions.ConstructionAction;
import pa.soft.model.ConstructionType;
import pa.soft.util.ResourceProvider;

public class ConstructionButton
{
	private static final double DEFAULT_SIZE = 32;
	private final ConstructionType constructionType;
	private final ResourceProvider resourceProvider = new ResourceProvider();

	private final Button viewNode = new Button();

	public ConstructionButton(ConstructionType constructionType)
	{
		super();

		this.constructionType = constructionType;

		viewNode.setText(constructionType.getName() + " bauen");
		ImageView imageView = generateImageView(constructionType);

		viewNode.setGraphic(imageView);
		viewNode.setMaxWidth(Double.MAX_VALUE);
	}

	private ImageView generateImageView(final ConstructionType constructionType)
	{
		ImageView imageView = new ImageView();
		imageView.setImage(resourceProvider.provideConstructionImage(constructionType.getIconFileName()));
		imageView.setFitWidth(DEFAULT_SIZE);
		imageView.setFitHeight(DEFAULT_SIZE);
		return imageView;
	}

	public ConstructionType getConstructionType()
	{
		return constructionType;
	}

	public void setDisable(final boolean disabled)
	{
		viewNode.setDisable(disabled);
	}

	public Button getViewNode()
	{
		return viewNode;
	}

	public void setConstruction(final ConstructionAction constructionAction, final ConstructionType constructionType)
	{
		viewNode.setOnAction(event -> constructionAction.construct(constructionType));
	}
}
