package pa.soft.view.instructions;

import javafx.scene.Scene;
import javafx.stage.Stage;

public class InstructionStage
{
	private final Stage dialog = new Stage();

	public void show()
	{
		MarkdownView markdownView = new MarkdownView();

		Scene dialogScene = new Scene(markdownView.getViewNode());
		dialog.setScene(dialogScene);
		dialog.show();
	}
}
