package pa.soft.view.instructions;

public class Instructions
{
	public static final String MARKDOWN = "## Menschen die Dörfer und Städte auf einer Insel bauen\n" + "#### UM WAS ES GEHT \n" +
			"Auf dem Bild ist eine Insel mit Symbolen für Straßen, Siedlungen, Städte \n" + "und Ritter abgebildet. Der Spieler versucht, im Laufe des Spiels möglichst viele \n" +
			"Straßen, Siedlungen, Städte und Ritter auf der Insel zu bauen.\n" + "Bauen heißt, das jeweilige Symbol wechselt von schwarzem Umriss zu grauen Konturen.\n" +
			"Bauen kostet Rohstoffe. Die Rohstoffe werden mit 6 Würfeln ausgewürfelt. Auf \n" + "jedem Würfel ist je 1x Wolle, Getreide, Lehm, Erz, Holz und Gold abgebildet. \n" +
			"\n" + "Der Bau einer Straße kostet beispielsweise je 1x Holz und Lehm. Nur dann, wenn \n" +
			"ein Spieler diese beiden Rohstoffe gewürfelt hat, kann er auch eine Straße bauen.\n" +
			"Wenn etwas gebaut wurde bekommt der Spieler so viele Punkte wie die Zahl auf dem \n" +
			"Symbol angibt. Einen Sonderfall stellt der Rohstoff Gold dar, der im Verhältnis \n" +
			"2:1 gegen einen anderen Rohstoff eingetauscht werden kann, so kann z.B. 1 Strasse\n" + "auch mit 1x Holz und 2x Gold gebaut werden.\n" + "\n" + "#### SPIELABLAUF\n" +
			"Der Spieler würfelt mit bis zu 6 Würfeln bis zu dreimal, einzelne Würfel können \n" +
			"verschlossen werden und werden dann nicht erneut geworfen. Anschließend baut er \n" +
			"mit den gewürfelten Rohstoffen und erhält die entsprechenden Punkte. Wenn der \n" +
			"Spieler in einer Runde nichts gebaut hat erhält er 2 Minuspunkte für diese Runde.\n" + "\n" + "#### ROHSTOFF-JOKER\n" +
			"Mit jedem gekauften Ritter wird ein Rohstoff-Joker freigeschaltet, der einmal \n" +
			"genutzt werden darf um einen erwürfelten Rohstoff in den angezeigten zu ändern.\n" + "Das Gold des letzten Ritters bringt einen Rohstoff freier Wahl.\n" + "\n" +
			"#### BAUREGELN - WAS DARF WO GEBAUT WERDEN?\n" + "######Straße: \n" + "Eine Straße kostet je 1x Holz und Lehm und ist immer 1 Punkt wert. Die erste Straße \n" +
			"(Startstraße) ist bereits errichtet, sie kostet keine Rohstoffe mehr. Straßen werden \n" +
			"fortlaufend gebaut, es muss also immer eine Straße gebaut werden, die an eine zuvor \n" +
			"gebaute Straße grenzt. Eine Siedlung oder Stadt, egal ob bereits gebaut oder nicht, \n" + "behindert den Weiterbau der Straße nicht.\n" + "\n" + "######Siedlung: \n" +
			"Eine Siedlung kostet je 1x Lehm, Holz, Wolle und Getreide. Eine Siedlung  kann nur \n" +
			"dann gebaut werden, wenn eine gebaute Straße an sie grenzt. Weiterhin gilt, dass \n" + "Siedlungen in der Reihenfolge ihrer Punkte gebaut werden.\n" + "\n" +
			"######Stadt: \n" + "Eine Stadt kostet 3x Erz und 2x Getreide. Es wird wie beim Bau einer Siedlung \n" +
			"verfahren: Eine Stadt darf erst dann gebaut werden, wenn eine gebaute Straße an sie \n" + "grenzt; auch Städte werden in der Reihenfolge ihrer Punkte gebaut.\n" +
			"\n" + "######Ritter: \n" + "Ein Ritter kostet je 1x Erz, Wolle und Getreide. Die Ritter werden in der Reihenfolge \n" +
			"ihrer Punktezahlen gebaut. Hat ein Spieler einen Ritter gebaut, so darf er einmal \n" +
			"im Spiel den Rohstoff unterhalb dieses Ritters als Joker einsetzen. Damit ein Ritter \n" +
			"gebaut werden kann, ist es nicht notwendig, dass eine Straße, Siedlung oder Stadt an \n" + "die entsprechende Landschaft angrenzt.";

	private Instructions()
	{}
}
