package pa.soft.view.instructions;

import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.tautua.markdownpapers.HtmlEmitter;
import org.tautua.markdownpapers.ast.Document;
import org.tautua.markdownpapers.parser.ParseException;
import org.tautua.markdownpapers.parser.Parser;
import pa.soft.util.Logger;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class MarkdownView
{
	private final Logger logger;

	private final WebView viewNode;

	public MarkdownView()
	{
		final String content = generateContent();
		viewNode = new WebView();
		final WebEngine webEngine = viewNode.getEngine();
		webEngine.loadContent(content, "text/html");
		logger = new Logger("DieIrgendwerVonIrgendwo.log");
	}

	private String generateContent()
	{
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("<html><head></head><body>");

		InputStream targetStream = new ByteArrayInputStream(Instructions.MARKDOWN.getBytes());
		parseDocument(targetStream, stringBuilder);

		stringBuilder.append("</body></html>");

		return stringBuilder.toString();
	}

	private void parseDocument(final InputStream in, final StringBuilder stringBuilder)
	{
		Parser parser = new Parser(in);
		Document document = null;
		try
		{
			document = parser.parse();
		}
		catch(ParseException e)
		{
			logger.logException(e);
		}
		assert document != null;
		document.accept(new HtmlEmitter(stringBuilder));
	}

	public WebView getViewNode()
	{
		return viewNode;
	}
}
