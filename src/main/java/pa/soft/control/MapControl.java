package pa.soft.control;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import pa.soft.control.actions.RitterBaustoffAction;
import pa.soft.model.ConstructionType;
import pa.soft.model.BaustoffType;
import pa.soft.view.playerview.map.Construction;
import pa.soft.view.playerview.MapView;
import pa.soft.view.playerview.map.RitterBaustoff;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MapControl
{
	private final ListProperty<Construction> strassen = new SimpleListProperty<>(FXCollections.observableArrayList());
	private final ListProperty<Construction> ritter = new SimpleListProperty<>(FXCollections.observableArrayList());
	private final ListProperty<Construction> siedlungen = new SimpleListProperty<>(FXCollections.observableArrayList());
	private final ListProperty<Construction> staedte = new SimpleListProperty<>(FXCollections.observableArrayList());
	private final List<RitterBaustoff> ritterBaustoffe = new ArrayList<>();

	private final RitterBaustoffAction ritterBaustoffAction;

	MapControl(final RitterBaustoffAction ritterBaustoffAction)
	{
		this.ritterBaustoffAction = ritterBaustoffAction;
	}

	public MapView getNewMapView()
	{
		final Pane strassenLayer = generateStrasseLayer();
		final Pane ritterLayer = generateRitterLayer();
		final Pane siedlungLayer = generateSiedlungLayer();
		final Pane stadtLayer = generateStadtLayer();
		final Pane resourcenLayer = generateResourceLayer();

		return new MapView(strassenLayer, ritterLayer, siedlungLayer, stadtLayer, resourcenLayer);
	}

	private Pane generateStrasseLayer()
	{
		final double rechtsHoch = -31;
		final double linkshoch = 31;
		final double senkrecht = -90;
		List<Double> rotations =
				Arrays.asList(linkshoch, senkrecht, rechtsHoch, linkshoch, senkrecht, rechtsHoch, senkrecht, linkshoch,
							  rechtsHoch, linkshoch, rechtsHoch, linkshoch, senkrecht, linkshoch, rechtsHoch, linkshoch);
		List<Point2D> positions = Arrays.asList(
				new Point2D(375, 185),
				new Point2D(320, 100),
				new Point2D(275, 185),
				new Point2D(170, 185),
				new Point2D(120, 100),
				new Point2D(70, 185),
				new Point2D(20, 275),
				new Point2D(65, 355),
				new Point2D(175, 355),
				new Point2D(265, 355),
				new Point2D(370, 355),
				new Point2D(470, 355),
				new Point2D(120, 445),
				new Point2D(165, 525),
				new Point2D(270, 525),
				new Point2D(365, 525)
		);

		strassen.clear();
		for (int i = 0; i < 16; i++)
		{
			Construction constructionStrasse = new Construction(ConstructionType.STRASSE, 1);
			constructionStrasse.setPosition(positions.get(i));
			constructionStrasse.setRotate(rotations.get(i));
			strassen.add(constructionStrasse);
		}

		Pane layer = new Pane();
		strassen.forEach(construction -> layer.getChildren().add(construction.getViewNode()));

		return layer;
	}

	private Pane generateRitterLayer()
	{
		List<Point2D> positions = Arrays.asList(
				new Point2D(420, 50),
				new Point2D(220, 50),
				new Point2D(120, 220),
				new Point2D(220, 390),
				new Point2D(420, 390),
				new Point2D(520, 220)
		);

		ritter.clear();
		for (int i = 0; i < 6; i++)
		{
			Construction constructionRitter = new Construction(ConstructionType.RITTER,i + 1);
			constructionRitter.setPosition(positions.get(i));
			ritter.add(constructionRitter);
		}

		Pane layer = new Pane();
		ritter.forEach(construction -> layer.getChildren().add(construction.getViewNode()));

		return layer;
	}

	private Pane generateSiedlungLayer()
	{
		List<Integer> punkte = Arrays.asList(3, 4, 5, 7, 9, 11);
		List<Point2D> positions = Arrays.asList(
				new Point2D(420, 210),
				new Point2D(220, 210),
				new Point2D(20, 210),
				new Point2D(125, 380),
				new Point2D(325, 380),
				new Point2D(525, 380)
		);

		siedlungen.clear();
		for (int i = 0; i < 6; i++)
		{
			Construction constructionSiedlung = new Construction(ConstructionType.SIEDLUNG, punkte.get(i));
			constructionSiedlung.setPosition(positions.get(i));
			siedlungen.add(constructionSiedlung);
		}
		Pane layer = new Pane();
		siedlungen.forEach(construction -> layer.getChildren().add(construction.getViewNode()));

		return layer;
	}

	private Pane generateStadtLayer()
	{
		List<Integer> punkte = Arrays.asList(7, 12, 20, 30);
		List<Point2D> positions = Arrays.asList(
				new Point2D(325, 40),
				new Point2D(125, 40),
				new Point2D(220, 545),
				new Point2D(420, 545)
		);

		staedte.clear();
		for (int i = 0; i < 4; i++)
		{
			Construction constructionSiedlung = new Construction(ConstructionType.STADT, punkte.get(i));
			constructionSiedlung.setPosition(positions.get(i));
			staedte.add(constructionSiedlung);
		}

		Pane layer = new Pane();
		staedte.forEach(construction -> layer.getChildren().add(construction.getViewNode()));

		return layer;
	}

	private Pane generateResourceLayer()
	{
		List<Point2D> positions = Arrays.asList(
				new Point2D(415, 100),
				new Point2D(215, 100),
				new Point2D(115, 270),
				new Point2D(215, 440),
				new Point2D(415, 440),
				new Point2D(515, 270)
		);
		List<BaustoffType> baustoffTypes = Arrays.asList(
				BaustoffType.STEIN,
				BaustoffType.GETREIDE,
				BaustoffType.SCHAF,
				BaustoffType.HOLZ,
				BaustoffType.LEHM,
				BaustoffType.GOLD
		);

		ritterBaustoffe.clear();
		for (int i = 0; i < 6; i++)
		{
			RitterBaustoff ritterBaustoff = new RitterBaustoff(baustoffTypes.get(i), ritterBaustoffAction);
			ritterBaustoff.setPosition(positions.get(i));
			ritterBaustoff.isActiveProperty().bind(ritter.get(i).isConstructed());
			ritterBaustoffe.add(ritterBaustoff);
		}

		Pane layer = new Pane();
		ritterBaustoffe.forEach(ritterBaustoff -> layer.getChildren().add(ritterBaustoff.getViewNode()));

		return layer;
	}

	ListProperty<Construction> strassenProperty()
	{
		return strassen;
	}

	ListProperty<Construction> ritterProperty()
	{
		return ritter;
	}

	ListProperty<Construction> siedlungenProperty()
	{
		return siedlungen;
	}

	ListProperty<Construction> staedteProperty()
	{
		return staedte;
	}
}
