package pa.soft.control;

import javafx.scene.layout.VBox;
import pa.soft.control.actions.ConstructionAction;
import pa.soft.model.ConstructionType;
import pa.soft.view.diceview.ConstructionButton;

import java.util.ArrayList;
import java.util.List;

public class ConstructionControl
{
	private final List<ConstructionButton> constructionButtonList = new ArrayList<>();
	private VBox constructionButtonView;
	private ConstructionAction constructionAction;

	public VBox getView()
	{
		if(constructionButtonView == null)
		{
			constructionButtonView = new VBox(5);
			generateConstructionButtonList();
			constructionButtonList.forEach(constructionButton -> constructionButtonView.getChildren().add(constructionButton.getViewNode()));
			constructionButtonView.setStyle("-fx-padding: 5px 0 5px 0");
		}

		return constructionButtonView;
	}

	private void generateConstructionButtonList()
	{
		constructionButtonList.clear();
		for(ConstructionType constructionType : ConstructionType.values())
		{
			final ConstructionButton constructionButton = new ConstructionButton(constructionType);
			constructionButton.setDisable(true);
			constructionButton.setConstruction(constructionAction, constructionType);
			constructionButtonList.add(constructionButton);
		}
	}

	void updateConstructionButtons(final List<ConstructionType> constructionTypes)
	{
		constructionButtonView.getChildren().clear();

		constructionButtonList
				.forEach(constructionButton -> {
					constructionButton.setDisable(!constructionTypes.contains(constructionButton.getConstructionType()));
					constructionButtonView.getChildren().add(constructionButton.getViewNode());
				});

	}

	void setConstructionAction(final ConstructionAction constructionAction)
	{
		this.constructionAction = constructionAction;
	}
}
