package pa.soft.control.actions;

import pa.soft.model.BaustoffType;

public interface RitterBaustoffAction
{
	boolean useRitterBaustoff(BaustoffType baustoffType);
}
