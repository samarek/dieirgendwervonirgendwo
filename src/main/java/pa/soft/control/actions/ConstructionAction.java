package pa.soft.control.actions;

import pa.soft.model.ConstructionType;

public interface ConstructionAction
{
	void construct(ConstructionType constructionType);
}
