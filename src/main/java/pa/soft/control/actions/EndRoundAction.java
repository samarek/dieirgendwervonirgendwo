package pa.soft.control.actions;

public interface EndRoundAction
{
	void endRound();
}
