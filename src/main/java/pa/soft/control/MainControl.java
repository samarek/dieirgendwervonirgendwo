package pa.soft.control;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import pa.soft.model.ConstructionType;
import pa.soft.view.MainView;

public class MainControl
{
	private MainView mainView;
	private PlayerControl playerControl;
	private GameControl gameControl;
	private DiceControl diceControl;

	public static Parent start()
	{
		MainControl mainControl = new MainControl();
		return (Parent) mainControl.getView();
	}

	public Node getView()
	{
		if(mainView == null)
		{
			MapControl mapControl = new MapControl(baustoffType -> diceControl.useRitterBaustoff(baustoffType));
			diceControl = new DiceControl();
			playerControl = new PlayerControl(mapControl);
			gameControl = new GameControl(playerControl.getMapControl());

			diceControl.setEndRoundAction(() -> playerControl.endRound(gameControl.fetchPoints()));
			diceControl.setConstructionAction(this::executeConstruction);
			mainView = new MainView(diceControl, playerControl);
		}
		return mainView.getViewNode();
	}

	private void executeConstruction(final ConstructionType constructionType)
	{
		if(gameControl.attemptConstruction(constructionType))
		{
			diceControl.removeBaustoffeForConstructionType(constructionType);
		}
		else if (!constructionType.equals(ConstructionType.STRASSE))
		{
			Alert noStreet = new Alert(Alert.AlertType.INFORMATION);
			noStreet.setHeaderText("");
			noStreet.setContentText("Sie benötigen zuerst eine Strasse zu dieser " + constructionType.getName());
			noStreet.show();
		}
	}
}
