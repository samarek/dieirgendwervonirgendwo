package pa.soft.control;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import pa.soft.view.PlayerView;

public class PlayerControl
{
	private static final int MAX_ROUNDS = 15;
	private int round = 0;
	private final PlayerView playerView;
	private final MapControl mapControl;

	PlayerControl(final MapControl mapControl)
	{
		this.mapControl = mapControl;
		this.playerView = new PlayerView(mapControl);
	}

	public Node getView()
	{
		return playerView.getViewNode();
	}

	void endRound(final int punkte)
	{
		if(round < MAX_ROUNDS)
		{
			playerView.setPunkteForRound(punkte, round++);
		}
		if(round == MAX_ROUNDS)
		{
			showGameOver();
			round = 0;
			playerView.reset();
		}
	}

	private void showGameOver()
	{
		Alert gameOver = new Alert(Alert.AlertType.INFORMATION);
		gameOver.setHeaderText("Game Over");
		gameOver.setContentText("Das Spiel ging über 15 Runden und ist nun zu Ende");
		gameOver.showAndWait();
	}

	MapControl getMapControl()
	{
		return mapControl;
	}
}
