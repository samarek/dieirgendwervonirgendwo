package pa.soft.control;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.MapProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.collections.FXCollections;
import javafx.scene.Node;
import pa.soft.control.actions.ConstructionAction;
import pa.soft.control.actions.EndRoundAction;
import pa.soft.model.BaustoffType;
import pa.soft.model.ConstructionType;
import pa.soft.view.DiceView;
import pa.soft.view.playerview.picker.BaustoffPicker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class DiceControl
{
	private final ConstructionControl constructionControl = new ConstructionControl();
	private final IntegerProperty rollsRemainingProperty = new SimpleIntegerProperty(3);
	private final MapProperty<BaustoffType, Integer> availableBaustoffAmountProperty = new SimpleMapProperty<>();
	private EndRoundAction endRoundAction;

	private DiceView diceView;

	DiceControl()
	{
		availableBaustoffAmountProperty.addListener((observable, oldValue, newValue) -> updateConstructionButtons());
	}

	public Node getView()
	{
		if(diceView == null)
		{
			diceView = new DiceView(this::rollDice, this::endRound, constructionControl);
			diceView.rollsRemainingProperty().bind(rollsRemainingProperty);
		}

		return diceView.getViewNode();
	}

	private void rollDice()
	{
		if(rollsRemainingProperty.getValue() > 0)
		{
			diceView.rollDice();
			updateAvailableBaustoffe();
			decrementRolls();
		}
	}

	private void updateConstructionButtons()
	{
		constructionControl.updateConstructionButtons(collectBuildables(availableBaustoffAmountProperty));
	}

	private void decrementRolls()
	{
		rollsRemainingProperty.setValue(rollsRemainingProperty.getValue() - 1);
	}

	private List<ConstructionType> collectBuildables(final Map<BaustoffType, Integer> baustoffTypes)
	{
		List<ConstructionType> constructionTypes = new ArrayList<>();
		for(ConstructionType constructionType : ConstructionType.values())
		{
			if(constructionType.isBuildableWith(baustoffTypes))
			{
				constructionTypes.add(constructionType);
			}
		}

		return constructionTypes;
	}

	private Map<BaustoffType, Integer> buildBaustoffMap(final List<BaustoffType> baustoffTypes)
	{
		Map<BaustoffType, Integer> resultMap = new EnumMap<>(BaustoffType.class);
		for(BaustoffType baustoffType : baustoffTypes)
		{
			int result = resultMap.getOrDefault(baustoffType, 0);
			result++;
			resultMap.put(baustoffType, result);
		}

		return resultMap;
	}

	void setEndRoundAction(EndRoundAction endRoundAction)
	{
		this.endRoundAction = endRoundAction;
	}

	void setConstructionAction(ConstructionAction constructionAction)
	{
		constructionControl.setConstructionAction(constructionAction);
	}

	void removeBaustoffeForConstructionType(ConstructionType constructionType)
	{
		diceView.removeBaustoffe(constructionType.getBaustoffe());

		removeBaustoffeFromProperty(constructionType.getBaustoffMap());
	}

	private void removeBaustoffeFromProperty(final Map<BaustoffType, Integer> baustoffMap)
	{
		int goldRequired = 0;
		for(Map.Entry<BaustoffType, Integer> baustoffTypeIntegerEntry : baustoffMap.entrySet())
		{
			BaustoffType keyToRemove = baustoffTypeIntegerEntry.getKey();

			if(availableBaustoffAmountProperty.containsKey(keyToRemove))
			{
				goldRequired += subtractBaustoff(baustoffTypeIntegerEntry.getValue(), keyToRemove);
			}
			else
			{
				goldRequired += baustoffTypeIntegerEntry.getValue() * 2;
			}
		}
		removeGold(goldRequired);
	}

	private int subtractBaustoff(final int amount, final BaustoffType baustoffType)
	{
		int availableValue = availableBaustoffAmountProperty.get(baustoffType) - amount;
		if(availableValue > 0)
		{
			availableBaustoffAmountProperty.put(baustoffType, availableValue);
			return 0;
		}
		else
		{
			availableBaustoffAmountProperty.remove(baustoffType);
			return Math.abs(availableValue) * 2;
		}
	}

	private void removeGold(final int goldRequired)
	{
		if (goldRequired > 0)
		{
			int amount = availableBaustoffAmountProperty.get(BaustoffType.GOLD) - goldRequired;
			availableBaustoffAmountProperty.put(BaustoffType.GOLD, amount);
		}
	}

	private void endRound()
	{
		resetView();
		endRoundAction.endRound();
	}

	private void resetView()
	{
		rollsRemainingProperty.setValue(3);
		diceView.resetResources();
		constructionControl.updateConstructionButtons(Collections.emptyList());
	}

	boolean useRitterBaustoff(final BaustoffType replaceWithBaustoffType)
	{
		BaustoffType replaceWithBaustoffTypeLocal = replaceWithBaustoffType.equals(BaustoffType.GOLD) ? pickReplaceWithBaustoffType() : replaceWithBaustoffType;
		if(replaceWithBaustoffTypeLocal.equals(BaustoffType.GOLD))
		{
			return false;
		}

		Map<Integer, BaustoffType> unusedBaustoffMap = makeUniqueUnusedBaustoffMap(replaceWithBaustoffType);

		BaustoffPicker baustoffPicker = new BaustoffPicker(unusedBaustoffMap);
		final Optional<Integer> result = baustoffPicker.showAndWait();
		result.ifPresent(integer -> {
			diceView.replaceBaustoff(unusedBaustoffMap.get(integer), replaceWithBaustoffTypeLocal);
			updateAvailableBaustoffe();
		});

		return result.isPresent() && result.get() > 0;
	}

	private BaustoffType pickReplaceWithBaustoffType()
	{
		final Map<Integer, BaustoffType> allBaustoffMap = makeBaustoffMap(BaustoffType.GOLD, Arrays.asList(BaustoffType.values()));
		BaustoffPicker baustoffPicker = new BaustoffPicker(allBaustoffMap);
		final Optional<Integer> result = baustoffPicker.showAndWait();

		return result.map(allBaustoffMap::get).orElse(BaustoffType.GOLD);
	}

	private void updateAvailableBaustoffe()
	{
		availableBaustoffAmountProperty.setValue(FXCollections.observableMap(buildBaustoffMap(diceView.getUnusedBaustoffe())));
	}

	private Map<Integer, BaustoffType> makeUniqueUnusedBaustoffMap(final BaustoffType replaceWithBaustoffType)
	{
		Set<BaustoffType> unusedBaustoffe = new HashSet<>(diceView.getUnusedBaustoffe());
		return makeBaustoffMap(replaceWithBaustoffType, unusedBaustoffe);
	}

	private Map<Integer, BaustoffType> makeBaustoffMap(final BaustoffType baustoffType, final Collection<BaustoffType> baustoffe)
	{
		Map<Integer, BaustoffType> baustoffMap = new HashMap<>();
		final AtomicInteger i = new AtomicInteger(0);
		baustoffe.stream()
				 .filter(baustoff -> !baustoff.equals(baustoffType))
				 .forEach(baustoff -> baustoffMap.put(i.incrementAndGet(), baustoff));

		return baustoffMap;
	}
}
