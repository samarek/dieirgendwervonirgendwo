package pa.soft.control;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import pa.soft.model.ConstructionType;
import pa.soft.view.playerview.map.Construction;
import pa.soft.view.playerview.picker.StrassenPicker;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class GameControl
{
	private static final int EVALUATION_ERROR = -1;

	private final ListProperty<Construction> strassen = new SimpleListProperty<>(FXCollections.observableArrayList());
	private final ListProperty<Construction> ritter = new SimpleListProperty<>(FXCollections.observableArrayList());
	private final ListProperty<Construction> siedlungen = new SimpleListProperty<>(FXCollections.observableArrayList());
	private final ListProperty<Construction> staedte = new SimpleListProperty<>(FXCollections.observableArrayList());

	private int rundenPunkte = 0;

	GameControl(final MapControl mapControl)
	{
		this.strassen.bind(mapControl.strassenProperty());
		this.ritter.bind(mapControl.ritterProperty());
		this.siedlungen.bind(mapControl.siedlungenProperty());
		this.staedte.bind(mapControl.staedteProperty());
	}

	boolean attemptConstruction(final ConstructionType constructionType)
	{
		switch(constructionType)
		{
			case STRASSE:
				return constructStrasse();
			case RITTER:
				return constructRitter();
			case SIEDLUNG:
				return attemptSiedlungConstruction();
			case STADT:
				return attemptStadtConstruction();
			default:
				return false;
		}
	}

	private boolean constructStrasse()
	{
		Construction buildableStrasse = evaluateStrassen();

		if(buildableStrasse != null)
		{
			rundenPunkte += buildableStrasse.construct();
			return true;
		}

		return false;
	}

	private Construction evaluateStrassen()
	{
		final Map<Integer, Construction> builableStrassen = new HashMap<>();
		for (Construction strasse : strassen)
		{
			if(!strasse.isConstructed().getValue() && isStrasseBuildable(strasse))
			{
				builableStrassen.put(strassen.indexOf(strasse), strasse);
			}
		}

		if(builableStrassen.size() > 1)
		{
			StrassenPicker strassenPicker = new StrassenPicker(builableStrassen);
			final Optional<Integer> result = strassenPicker.showAndWait();

			return builableStrassen.get(result.orElse(0));
		}
		else
		{
			return extractSingleElement(builableStrassen);
		}
	}

	private Construction extractSingleElement(final Map<Integer, Construction> builableStrassen)
	{
		return builableStrassen.values().iterator().hasNext() ? builableStrassen.values().iterator().next() : null;
	}

	private boolean isStrasseBuildable(Construction strasse)
	{
		int strassenIndex = strassen.indexOf(strasse);
		switch(strassenIndex)
		{
			case 1:
			case 2:
				return strassen.get(0).isConstructed().getValue();
			case 3:
				return strassen.get(2).isConstructed().getValue();
			case 4:
			case 5:
				return strassen.get(3).isConstructed().getValue();
			case 6:
				return strassen.get(5).isConstructed().getValue();
			case 7:
				return strassen.get(6).isConstructed().getValue();
			case 8:
			case 12:
				return strassen.get(7).isConstructed().getValue();
			case 9:
				return strassen.get(8).isConstructed().getValue();
			case 10:
				return strassen.get(9).isConstructed().getValue();
			case 11:
				return strassen.get(10).isConstructed().getValue();
			case 13:
				return strassen.get(12).isConstructed().getValue();
			case 14:
				return strassen.get(13).isConstructed().getValue();
			case 15:
				return strassen.get(14).isConstructed().getValue();
			default:
				return true;
		}
	}

	private boolean constructRitter()
	{
		int buildableRitterIndex = evaluateRitter();
		if(buildableRitterIndex >= 0)
		{
			rundenPunkte += ritter.get(buildableRitterIndex).construct();
			return true;
		}

		return false;
	}

	private int evaluateRitter()
	{
		final Optional<Construction> firstRitterOptional = ritter.stream()
																 .filter(construction -> !construction.isConstructed().getValue())
																 .findFirst();
		return firstRitterOptional.map(ritter::indexOf).orElse(EVALUATION_ERROR);
	}

	private boolean attemptSiedlungConstruction()
	{
		int buildableSiedlungIndex = evaluateSiedlung();
		if(buildableSiedlungIndex >= 0)
		{
			rundenPunkte += siedlungen.get(buildableSiedlungIndex).construct();
			return true;
		}

		return false;
	}

	private int evaluateSiedlung()
	{
		final Optional<Construction> firstSiedlungOptional = siedlungen.stream()
																	   .filter(construction -> !construction.isConstructed().getValue())
																	   .findFirst();
		if(firstSiedlungOptional.isPresent())
		{
			final int firstStadtIndex = siedlungen.indexOf(firstSiedlungOptional.get());
			final Construction matchingStrasse = findMatchingStrasseForSiedlung(firstStadtIndex);

			if(firstStadtIndex == 0 || matchingStrasse.isConstructed().get())
			{
				return firstStadtIndex;
			}
		}

		return EVALUATION_ERROR;
	}

	private Construction findMatchingStrasseForSiedlung(final int firstSiedlungIndex)
	{
		switch(firstSiedlungIndex)
		{
			case 1:
				return strassen.get(2);
			case 2:
				return strassen.get(5);
			case 3:
				return strassen.get(7);
			case 4:
				return strassen.get(9);
			case 5:
			default:
				return strassen.get(11);
		}
	}

	private boolean attemptStadtConstruction()
	{
		int buildableStadtIndex = evaluteStadt();
		if(buildableStadtIndex >= 0)
		{
			rundenPunkte += staedte.get(buildableStadtIndex).construct();
			return true;
		}

		return false;
	}

	private int evaluteStadt()
	{
		final Optional<Construction> firstStadtOptional = staedte.stream()
																 .filter(construction -> !construction.isConstructed().getValue())
																 .findFirst();
		if(firstStadtOptional.isPresent())
		{
			final int firstStadtIndex = staedte.indexOf(firstStadtOptional.get());
			final Construction matchingStrasse = findMatchingStrasseForStadt(firstStadtIndex);

			if(matchingStrasse.isConstructed().get())
			{
				return firstStadtIndex;
			}
		}

		return EVALUATION_ERROR;
	}

	private Construction findMatchingStrasseForStadt(final int firstStadtIndex)
	{
		switch(firstStadtIndex)
		{
			case 0:
				return strassen.get(1);
			case 1:
				return strassen.get(4);
			case 2:
				return strassen.get(13);
			case 3:
			default:
				return strassen.get(15);
		}
	}

	int fetchPoints()
	{
		int punkte = rundenPunkte == 0 ? -2 : rundenPunkte;
		rundenPunkte = 0;

		return punkte;
	}
}
