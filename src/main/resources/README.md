## Menschen die Dörfer und Städte auf einer Insel bauen
#### UM WAS ES GEHT 
Auf dem Bild ist eine Insel mit Symbolen für Straßen, Siedlungen, Städte 
und Ritter abgebildet. Der Spieler versucht, im Laufe des Spiels möglichst viele 
Straßen, Siedlungen, Städte und Ritter auf der Insel zu bauen.
Bauen heißt, das jeweilige Symbol wechselt von schwarzem Umriss zu grauen Konturen.
Bauen kostet Rohstoffe. Die Rohstoffe werden mit 6 Würfeln ausgewürfelt. Auf 
jedem Würfel ist je 1x Wolle, Getreide, Lehm, Erz, Holz und Gold abgebildet. 

Der Bau einer Straße kostet beispielsweise je 1x Holz und Lehm. Nur dann, wenn 
ein Spieler diese beiden Rohstoffe gewürfelt hat, kann er auch eine Straße bauen.
Wenn etwas gebaut wurde bekommt der Spieler so viele Punkte wie die Zahl auf dem 
Symbol angibt. Einen Sonderfall stellt der Rohstoff Gold dar, der im Verhältnis 
2:1 gegen einen anderen Rohstoff eingetauscht werden kann, so kann z.B. 1 Strasse
auch mit 1x Holz und 2x Gold gebaut werden.

#### SPIELABLAUF
Der Spieler würfelt mit bis zu 6 Würfeln bis zu dreimal, einzelne Würfel können 
verschlossen werden und werden dann nicht erneut geworfen. Anschließend baut er 
mit den gewürfelten Rohstoffen und erhält die entsprechenden Punkte. Wenn der 
Spieler in einer Runde nichts gebaut hat erhält er 2 Minuspunkte für diese Runde.

#### ROHSTOFF-JOKER
Mit jedem gekauften Ritter wird ein Rohstoff-Joker freigeschaltet, der einmal 
genutzt werden darf um einen erwürfelten Rohstoff in den angezeigten zu ändern.
Das Gold des letzten Ritters bringt einen Rohstoff freier Wahl.

#### BAUREGELN - WAS DARF WO GEBAUT WERDEN?
######Straße: 
Eine Straße kostet je 1x Holz und Lehm und ist immer 1 Punkt wert. Die erste Straße 
(Startstraße) ist bereits errichtet, sie kostet keine Rohstoffe mehr. Straßen werden 
fortlaufend gebaut, es muss also immer eine Straße gebaut werden, die an eine zuvor 
gebaute Straße grenzt. Eine Siedlung oder Stadt, egal ob bereits gebaut oder nicht, 
behindert den Weiterbau der Straße nicht.

######Siedlung: 
Eine Siedlung kostet je 1x Lehm, Holz, Wolle und Getreide. Eine Siedlung  kann nur 
dann gebaut werden, wenn eine gebaute Straße an sie grenzt. Weiterhin gilt, dass 
Siedlungen in der Reihenfolge ihrer Punkte gebaut werden.

######Stadt: 
Eine Stadt kostet 3x Erz und 2x Getreide. Es wird wie beim Bau einer Siedlung 
verfahren: Eine Stadt darf erst dann gebaut werden, wenn eine gebaute Straße an sie 
grenzt; auch Städte werden in der Reihenfolge ihrer Punkte gebaut.

######Ritter: 
Ein Ritter kostet je 1x Erz, Wolle und Getreide. Die Ritter werden in der Reihenfolge 
ihrer Punktezahlen gebaut. Hat ein Spieler einen Ritter gebaut, so darf er einmal 
im Spiel den Rohstoff unterhalb dieses Ritters als Joker einsetzen. Damit ein Ritter 
gebaut werden kann, ist es nicht notwendig, dass eine Straße, Siedlung oder Stadt an 
die entsprechende Landschaft angrenzt.